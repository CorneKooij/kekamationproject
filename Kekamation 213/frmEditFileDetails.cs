﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kekamation_213
{
    public partial class frmEditFileDetails : Form
    {
        string fileName;

        public frmEditFileDetails()
        {
            InitializeComponent();
        }

        public frmEditFileDetails(string name)
        {
            fileName = name;

            InitializeComponent();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            string name;

            name = txtName.Text;
            
            string[] headerKey = { "sql" };
            string[] headerVal = { "UPDATE user_details SET file_name = '" + name + "'" };
            serverLink temp = new serverLink();
            string tmp = temp.sendToServer("POST", "/api/admin", headerKey, headerVal);
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmEditFileDetails_Load(object sender, EventArgs e)
        {
            string[] headerKey = { "sql" };
            string[] headerVal = { "SELECT * FROM file_details WHERE file_name = '" + fileName + "'" };
            serverLink temp = new serverLink();
            string tmp = temp.sendToServer("POST", "/api/admin", headerKey, headerVal);

            //insert fields in user details into the text boxes
        }
    }
}
