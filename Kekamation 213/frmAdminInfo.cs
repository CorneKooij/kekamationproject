﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kekamation_213
{
    public partial class frmAdminInfo : Form
    {
        string mail;
        private const string sqlSelectAll = "SELECT * FROM file_details ";

        public frmAdminInfo()
        {
            InitializeComponent();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            mail = lbxUsers.SelectedItem.ToString();

            string[] headerKey = { "sql" };
            string[] headerVal = { "DELETE FROM user_details WHERE user_email = '" + mail + "';" };
            serverLink temp = new serverLink();
            string tmp = temp.sendToServer("POST", "/api/admin", headerKey, headerVal);
            MessageBox.Show(tmp);
            loadDetails();
        }

        private void frmAdminInfo_Load(object sender, EventArgs e)
        {
            loadDetails();
        }

        private void btnEditUser_Click(object sender, EventArgs e)
        {
            mail = lbxUsers.SelectedItem.ToString();

            frmEditUser edit = new frmEditUser(mail);
            edit.ShowDialog();
        }

        public void loadDetails()
        {
            lbxUsers.Items.Clear();
            string[] headerKey = { "sql" };
            string[] headerVal = { "SELECT user_email FROM user_details" };
            serverLink temp = new serverLink();
            string tmp = temp.sendToServer("POST", "/api/admin", headerKey, headerVal);

            string[] arr = tmp.Split('"');
            int k = 5;
            while (k < arr.Length && !arr[k].Equals("}]}"))
            {
                lbxUsers.Items.Add(arr[k]);
                k += 4;
            }
        }
    }
}
