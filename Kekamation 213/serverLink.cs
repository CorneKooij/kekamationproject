﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kekamation_213
{
    class serverLink
    {
        public String sendToServer(string method, string urlEx, string[] headerKey, string[] headerVal)
        {
            try
            {
                string URL = "http://34.240.65.169:8080" + urlEx;
                var request = WebRequest.Create(URL);
                request.ContentType = "application/json; charset=utf-8";
                string text;
                request.Method = method;

                for (int i = 0; i < headerKey.Length; i++)
                {
                    request.Headers.Add(headerKey[i], headerVal[i]);
                }

                var response = (HttpWebResponse)request.GetResponse();

                using (var sr = new StreamReader(response.GetResponseStream()))
                {
                    text = sr.ReadToEnd();
                }

                return text;
            }
            catch (Exception dex)
            {
                MessageBox.Show("error");
                return dex.Message;
            }
        }
    }
}
