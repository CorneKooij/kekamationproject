﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kekamation_213
{
    public partial class frmEditUser : Form
    {
        string email;

        public frmEditUser()
        {
            InitializeComponent();
        }

        public frmEditUser(string mail)
        {
            email = mail;

            InitializeComponent();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmEditUser_Load(object sender, EventArgs e)
        {
            string[] headerKey = { "sql" };
            string[] headerVal = { "SELECT * FROM user_details WHERE user_email = '" + email + "'" };
            serverLink temp = new serverLink();
            string tmp = temp.sendToServer("POST", "/api/admin", headerKey, headerVal);

            string[] arr = tmp.Split('"');

            txtName.Text = arr[7];
            txtSurname.Text = arr[11];
            txtEmail.Text = arr[15];
            
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            string name, surname, mail, pass;

            name = txtName.Text;
            surname = txtSurname.Text;
            mail = txtEmail.Text;
            pass = txtPass.Text;

            if (pass == txtConfirm.Text && pass.Length > 8)
            {
                string[] headerKey = { "sql" };
                string[] headerVal = { "UPDATE user_details SET user_name = '" + name + "', user_surname = '" + surname + "', user_email = '" + mail + "', user_password = '" + pass + "' WHERE user_email = '" + email + "'" };
                serverLink temp = new serverLink();
                string tmp = temp.sendToServer("POST", "/api/admin", headerKey, headerVal);
            }
            else
            {
                MessageBox.Show("Passwords must match and must be longer than 8 characters", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
