﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kekamation_213
{
    public partial class frmContentFilter : Form
    {
        public string Sql { get; set; }

        public frmContentFilter()
        {
            InitializeComponent();
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
            switch (cbxFilter.SelectedIndex)
            {
                case 0:
                    Sql = "ORDER BY file_date_added";
                    break;
                case 1:
                    Sql = "ORDER BY file_name";
                    break;
                case 2:
                    Sql = "ORDER BY file_size";
                    break;
                case 3:
                    Sql = "ORDER BY file_extension";
                    break;
                case 4:
                    Sql = "ORDER BY user_details_user_id";
                    break;
                default:
                    Sql = "ORDER BY file_date_added";
                    break;
            }

            if (rtnAsc.Checked)
                Sql += " ASC";
            if (rtnDesc.Checked)
                Sql += " DESC";
            Close();
        }
    }
}
