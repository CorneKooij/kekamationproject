﻿using Kekamation_213;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MovieBase_v0
{
    public partial class userLogin : Form
    {

        private string email;
        private string password;
        private frmSplash splash;

        public userLogin()
        {
            InitializeComponent();
            splash = new frmSplash();
        }

        //shows new accound form
        private void btnNew_Click(object sender, EventArgs e)
        {
            frmNewAccount temp = new frmNewAccount();
            temp.ShowDialog();
        }

        //Logs the user into the application
        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                password = txbPass.Text;
                email = txbEmail.Text;
            }
            catch (Exception ee)
            { MessageBox.Show(ee.Message, "Error",MessageBoxButtons.OK,MessageBoxIcon.Error); }

            if (validation())//Check if all the details of the user are usable if they are adds the user to the database
            {
                string val="";
                string[] headerKey = {"password", "email" };
                string[] headerVal = {password, email};
                serverLink temp = new serverLink();
                //splash.Show();
                try
                {
                    val = temp.sendToServer("POST", "/api/login", headerKey, headerVal);
                    //splash.Close();
                }
                catch(Exception ee)
                { MessageBox.Show(ee.Message); }
               
                if (val.Contains("success"))
                {
                    Hide();
                    Form1 main = new Form1(email);
                    main.ShowDialog();
                    Show();
                }
                else if (val.Contains("email"))
                {
                    if (MessageBox.Show("Resend verification email?", "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        string tmp = temp.sendToServer("POST", "/api/resendemail", headerKey, headerVal);
                        MessageBox.Show(tmp.Contains("email") ? "Email sent" : "Error");
                    }
                }
                else
                    MessageBox.Show("Invalid login");
            }
        }

        public bool validation()
        {
            if (txbPass.Text == "")
            {
                MessageBox.Show("Error: Fields can't be empty");
                return false;
            }
            if (!(email.IndexOf('@') != -1 && email.Length - email.IndexOf('@') > 3))
            {
                MessageBox.Show("Enter a valid email.");
                return false;
            }
            return true;
        }

        private void picBtnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
