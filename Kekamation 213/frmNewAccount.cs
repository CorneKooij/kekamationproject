﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using Kekamation_213;

namespace MovieBase_v0
{
    public partial class frmNewAccount : Form
    {
        //private variables
        private DateTime dateOfBirth;
        private string name;
        private string surname;
        private string password;
        private string confirmPassword;
        private string email;
        private bool admin = false;

        public frmNewAccount()
        {
            InitializeComponent();
        }

        //Constructor
        public frmNewAccount(bool isAdmin)
        {
            InitializeComponent();
            if (isAdmin)
                admin = true;
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        //Creates user account in the database
        private void button1_Click(object sender, EventArgs e)
        {
            try 
            {                       
                name = tbxName.Text;
                surname = tbxSurname.Text;
                password = tbxPassword.Text;
                confirmPassword = tbxConfirmPassword.Text;
                email = tbxEmail.Text;
            }
            catch(Exception ee)
            {   MessageBox.Show(ee.Message); }

            if (doDetailsWork())//Check if all the details of the user are usable if they are adds the user to the database
            {
                string[] headerKey = { "name", "surname", "password", "email", "isAdmin" };
                string[] headerVal = { name, surname, password, email, "0" };
                serverLink temp = new serverLink();
                string val = temp.sendToServer("POST", "/api/registeruser", headerKey, headerVal);
                MessageBox.Show(val);
                if (val.Contains("success"))
                {
                    MessageBox.Show("Account created. Check email");
                    Close();
                }
            }

        }

        //Checks if all the user details are usable
        private bool doDetailsWork()
        {
            try
            {
                if (!emailNotUsed())
                    throw new Exception("Error: Email is already used.");
                if (!validEmail())
                    throw new Exception("Error: Email is not valid.");
                if (!notEmpty(name))
                    throw new Exception("Error: Name can't be empty.");
                if (!notEmpty(surname))
                    throw new Exception("Error: Surname can't be empty.");
                if (!notEmpty(password))
                    throw new Exception("Error: Password can't be empty.");
                if (!passwordMatch())
                    throw new Exception("Error: The passwords don't match");

                return true;
            }
            catch(Exception ee)
            {
                MessageBox.Show(ee.Message);
                return false;
            }
        }

        //Checks if the email is already used
        private bool emailNotUsed()
        { 
            string[] headerKey = {"email"};
            string[] headerVal = {email};
            serverLink temp = new serverLink();
            string val = temp.sendToServer("POST", "/api/checkemail", headerKey, headerVal);

            if (val.Contains("available"))
            {
                return true;
            }
            else if (val.Contains("taken"))
            {
                return false;
            }
            else
            {
                MessageBox.Show("Check connection!");
                return false;
            }
        }

        //checks if the email is valid
        private bool validEmail()
        {
            if (email.IndexOf('@') != -1 && email.Length - email.IndexOf('@') > 3)
            {
                return true;
            }
            else return false;
        }

        //Checks if the string is not empty
        private bool notEmpty(string s)
        {
            if (s == "")
                return false;
            return true;
        }       

        //Checks if the 2 inserted passwords are the same
        private bool passwordMatch()
        {
            if (password == confirmPassword)
                return true;
            else
                return false;
        }
       

    }
}
          