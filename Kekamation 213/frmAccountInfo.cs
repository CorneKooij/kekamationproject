﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kekamation_213
{
    public partial class frmAccountInfo : Form
    {
        string fileName;
        string userid;

        public frmAccountInfo(string user_id)
        {
            userid = user_id;
            InitializeComponent();
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {

            fileName = lbxUploaded.SelectedItem.ToString();
            if (MessageBox.Show("Delete " + fileName + "?", "Confirm Download", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                string[] headerKey = { "sql" };
                string[] headerVal = { "DELETE FROM file_details WHERE file_name = '" + fileName + "'" };
                serverLink temp = new serverLink();
                string tmp = temp.sendToServer("POST", "/api/admin", headerKey, headerVal);
            }
            refresh();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            frmEditFileDetails editDetails = new frmEditFileDetails();
            editDetails.ShowDialog();
        }

        private void frmAccountInfo_Load(object sender, EventArgs e)
        {
            refresh();
        }
    
        public void refresh()
        {
            lbxUploaded.Items.Clear();
            string[] headerKey = { "sql" };
            string[] headerVal = { "SELECT file_name FROM file_details WHERE user_details_user_id = '"+userid+"'" };
            serverLink temp = new serverLink();
            string tmp = temp.sendToServer("POST", "/api/admin", headerKey, headerVal);
            string[] arr = tmp.Split('"');
            int k = 5;
            while (k < arr.Length && !arr[k].Equals("}]}"))
            {
                lbxUploaded.Items.Add(arr[k]);
                k += 4;
            }
        }
    }
}
