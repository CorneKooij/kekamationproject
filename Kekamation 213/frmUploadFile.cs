﻿using file_transfer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kekamation_213
{
    
    public partial class frmUploadFile : Form
    {

        private string filePath;
        private TransferClient transferClient;
        private const string SERVER_IP = "34.240.65.169";//"34.253.45.82";
        private const int SERVER_PORT = 8000; //Open ports 8000-8020
        private FileInfo oFileInfo;
        private string users;
        private string email;
        private Listener listener;
        private Timer tmrOverallProg;

        public frmUploadFile()
        {
            InitializeComponent();
        }

        public frmUploadFile(string filePath, string email)
        {
            InitializeComponent();
            this.filePath = filePath;
            this.email = email;
            tbxFilePath.Text = string.Join(" ;", filePath);
            transferClient = new TransferClient();
            transferClient.Connect(SERVER_IP.Trim(), SERVER_PORT, connectCallback);
            registerEvents();
            rdbAll.Checked = true;

            refreshText();

            InitializeComponent();
            listener = new Listener();
            listener.Accepted += listener_Accepted;

            tmrOverallProg = new System.Windows.Forms.Timer();
            tmrOverallProg.Interval = 1000;
            tmrOverallProg.Tick += tmrOverallProg_Tick;
           // btnSendFile.Click += new EventHandler(btnSendFile_Click);
        }

        //Adds user
        private void btnAddUser_Click(object sender, EventArgs e)
        {
            
            refreshText();
        }

        //Refreshes textbox
        private void refreshText()
        {
            oFileInfo = new FileInfo(filePath);
            tbxUpload.Text = "";
            tbxUpload.Text += "File Name: " + Path.GetFileNameWithoutExtension(filePath);
            tbxUpload.AppendText(Environment.NewLine+"File Size: " + oFileInfo.Length);
            tbxUpload.AppendText(Environment.NewLine + "File Extension: " + Path.GetExtension(filePath));
            tbxUpload.AppendText(Environment.NewLine + "Users: " + checkedRadioButton());
        }

        //Uploads the file
        private void button1_Click(object sender, EventArgs e)
        {
            oFileInfo = new FileInfo(filePath);
            try
            {
                transferClient.QueueTransfer(filePath);
            }
            catch (Exception ee)
            { MessageBox.Show(ee.Message, "Connection error", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        private void tmrOverallProg_Tick(object sender, EventArgs e)
        {
            if (transferClient == null)
                return;
        }

        private void listener_Accepted(object sender, SocketAcceptedEventArgs e)
        {
            if (InvokeRequired)
            {
                Invoke(new SocketAcceptedHandler(listener_Accepted), sender, e);
                return;
            }

            listener.Stop();

            transferClient = new TransferClient(e.Accepted);
            transferClient.OutputFolder = filePath;

            registerEvents();
            transferClient.Run();
            tmrOverallProg.Start();
        }

        private void connectCallback(object sender, string error)
        {
            if (InvokeRequired)
            {
                Invoke(new ConnectCallback(connectCallback), sender, error);
                return;
            }

            Enabled = true;

            if (error != null)
            {
                transferClient.Close();
                transferClient = null;
                MessageBox.Show(error, "Connection error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            registerEvents();          
            transferClient.Run();         
            tmrOverallProg.Start();
           
        }

        private void registerEvents()
        {
            transferClient.Complete += transferClient_Complete;
            transferClient.Disconnected += transferClient_Disconnected;
            transferClient.ProgressChanged += transferClient_ProgressChanged;
        }

        private void transferClient_Stopped(object sender, TransferQueue queue)
        {
            if (InvokeRequired)
            {
                Invoke(new TransferEventHandler(transferClient_Stopped), sender, queue);
                return;
            }
            Close();
        }

        private void transferClient_Queued(object sender, TransferQueue queue)
        {
            if (InvokeRequired)
            {
                Invoke(new TransferEventHandler(transferClient_Queued), sender, queue);
                return;
            }

            if (queue.Type == QueueType.Download)
            {
                transferClient.StartTransfer(queue);
            }
        }

        private void transferClient_ProgressChanged(object sender, TransferQueue queue)
        {
           
            if (InvokeRequired)
            {
                
                    Invoke(new TransferEventHandler(transferClient_ProgressChanged), sender, queue);
                    return;             
            }
           
            lblUpload.Text = "Uploaded: " + queue.Progress + "%";
            if (queue.Progress == 100)
            {
                queue.Progress = 101;
                string[] headerKey = { "sql" };
                string[] headerVal = { "SELECT * FROM user_details WHERE user_email = '" + email + "'" };
                serverLink temp = new serverLink();
                string tmp = temp.sendToServer("POST", "/api/admin", headerKey, headerVal);
                string[] arr = tmp.Split('"');
                arr[4] = arr[4].Remove(0, 1);
                arr[4] = arr[4].Remove(arr[4].Length - 1, 1);
                Console.Write("arr4 " + arr[4]);
                string[] headerKey1 = {"user", "name", "size", "extension" };
                string[] headerVal1 = { arr[4], oFileInfo.Name, oFileInfo.Length.ToString(), oFileInfo.Extension};
                tmp = temp.sendToServer("POST", "/api/newfile", headerKey1, headerVal1);
  
                MessageBox.Show("Uploaded " + oFileInfo.Name + "!","Success",MessageBoxButtons.OK,MessageBoxIcon.Information);
                Close();
            }
        }



        private void transferClient_Disconnected(object sender, EventArgs e)
        {
            if (InvokeRequired)
            {
                Invoke(new EventHandler(transferClient_Disconnected), sender, e);
                return;
            }
            deregisterEvents();
        }

        private void transferClient_Complete(object sender, TransferQueue queue)
        {
            System.Media.SystemSounds.Asterisk.Play();
           
        }

        private void deregisterEvents()
        {
            if (transferClient == null)
                return;
            transferClient.Complete -= transferClient_Complete;
            transferClient.Disconnected -= transferClient_Disconnected;
            transferClient.ProgressChanged -= transferClient_ProgressChanged;
            transferClient.Queued -= transferClient_Queued;
            transferClient.Stopped -= transferClient_Stopped;
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void rdbAll_Click(object sender, EventArgs e)
        {
            refreshText();
        }

        private void rdbAdmin_CheckedChanged(object sender, EventArgs e)
        {
            refreshText();
        }

        private void rdbAll_CheckedChanged(object sender, EventArgs e)
        {
            refreshText();
        }

        private void rdbMe_CheckedChanged(object sender, EventArgs e)
        {
            refreshText();
        }

        public string checkedRadioButton()
        {
            if (rdbAdmin.Checked)
            {
                return "Admin only";
            }
            else if (rdbAll.Checked)
            {
                return "All users";
            }
            else
            {
                return "Only me";
            }
        }

        private void rdbMe_Click(object sender, EventArgs e)
        {
            refreshText();
        }

        private void rdbAdmin_Click(object sender, EventArgs e)
        {
            refreshText();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
