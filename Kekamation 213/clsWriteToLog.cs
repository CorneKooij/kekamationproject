﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace MovieBase_v0
{
    class clsWriteToLog
    {
        //write property
        private string write { get; set; }

        //Writes to the log.txt file and calls the writeToFile method
        public clsWriteToLog(string input)
        {
            write = input;
            writeToFile();
        }


        //Writes the message to the log.txt file
        private void writeToFile()
        {
            try
            {
                using(StreamWriter writer = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "log.txt", true))
                {
                    writer.WriteLine(write + "\t" + DateTime.Now);
                }
            }
            catch (Exception dex)
            {
                MessageBox.Show("Could not write to log", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
