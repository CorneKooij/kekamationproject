﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Kekamation_213
{
    public partial class frmTransferSettings : Form
    {
        private string path;
        public frmTransferSettings()
        {
            InitializeComponent();
            path = "";
            System.IO.StreamReader file = new StreamReader("Settings");
            path = file.ReadLine();
            if (path == null)
                path = "Transfers";
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            tbxFolderPath.Text = Path.GetFullPath(path);
            file.Close();
        }

        private void btnChangePath_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if(fbd.ShowDialog()==DialogResult.OK)
            {
                path = fbd.SelectedPath;
            }
            tbxFolderPath.Text = path;
            File.WriteAllText("Settings", path);
        }

        private void btnClearDownFold_Click(object sender, EventArgs e)
        {
           if( MessageBox.Show("Are you sure you want to delete everything?", "Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Question)==DialogResult.Yes)
            {
                DirectoryInfo dir = new DirectoryInfo(path);
                dir.Delete(true);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
