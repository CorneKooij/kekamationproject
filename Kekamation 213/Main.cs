﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MovieBase_v0
{
    public partial class Main : Form
    {
        //variables and constructor
        private frmListMovies tempMovieFrm;
        private frmYoutubeTrailer tempYoutubeFrm;
        private frmUserDB tempUser;
        private frmLog tempLog;
        public bool isAdmin;

        public Main(bool admin)
        {
            InitializeComponent();
            tempMovieFrm = new frmListMovies("",this);
            tempYoutubeFrm = new frmYoutubeTrailer();
            tempLog = new frmLog();
            tempUser = new frmUserDB();
            frmLogin asd = new frmLogin();
            asd.Hide();
            isAdmin = admin;
        }

        //Closes application
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void listToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void watchTrailerToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        //Shows user form as mdi child
        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (isAdmin)
            {
                closeForms();
                tempUser = new frmUserDB();
                tempUser.MdiParent = this;
                tempUser.Show();
            }
            else
            {
                MessageBox.Show("You require admin privelage to access this table.", "ERROR",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
        }

        //Shows menu form as mdi child
        private void moviesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            closeForms();
            tempMovieFrm = new frmListMovies("",this);
            tempMovieFrm.MdiParent = this;
            tempMovieFrm.Show();
        }

        //Closes all the open forms
        public void closeForms()
        {
            tempMovieFrm.Close();
            tempLog.Close();
            tempUser.Close();
            tempYoutubeFrm.Close();
        }

        //Opens log form as mdi child
        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            if (isAdmin)
            {
                closeForms();
                tempLog = new frmLog();
                tempLog.MdiParent = this;
                tempLog.Show();
            }
            else
            {
                MessageBox.Show("You require admin privelage to access the log.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Checks if movie is rentable
        private void availableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            closeForms();
            tempMovieFrm = new frmListMovies("SELECT * FROM MOVIES WHERE MV_AVAILABLE <> 0;", this);
            tempMovieFrm.MdiParent = this;
            tempMovieFrm.Show();
        }

        //Checks if movie is rented out
        private void rentedOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            closeForms();
            tempMovieFrm = new frmListMovies("SELECT * FROM MOVIES WHERE MV_AVAILABLE = 0;",this);
            tempMovieFrm.MdiParent = this;
            tempMovieFrm.Show();
        }

        //Shows movies form as mdi child 
        private void searchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            closeForms();
            tempMovieFrm = new frmListMovies(null,this);
            tempMovieFrm.MdiParent = this;
            tempMovieFrm.Show();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            if(!isAdmin)
            {
                toolStripMenuItem2.Visible = false;
                toolStripMenuItem1.Visible = false;
            }
        }
    }
}
