﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kekamation_213
{
    public partial class downloadedlist : Form
    {
        private string userid;
        public downloadedlist(string user_id)
        {
            userid = user_id;
            InitializeComponent();
        }

        private void downloadedlist_Load(object sender, EventArgs e)
        {
            loadListView();
        }

        public void sqlStatement(string query)
        {
            listView1.Items.Clear();

            string[] headerKey = { "sql" };
            string[] headerVal = { query };
            serverLink temp = new serverLink();
            string tmp = temp.sendToServer("POST", "/api/admin", headerKey, headerVal);

            string[] arr = tmp.Split('"');

            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine("" + i + ": " + arr[i]);
            }

            int k = 4;
            while ((k + 2) < arr.Length && !arr[k].Equals("}]}"))
            {

                arr[k] = arr[k].Remove(0, 1);
                arr[k] = arr[k].Remove(arr[k].Length - 1, 1);
                arr[k + 2] = arr[k + 2].Remove(0, 1);
                arr[k + 2] = arr[k + 2].Remove(arr[k + 2].Length - 3, 3);

                string[] headerKey1 = { "sql" };
                string[] headerVal1 = { "SELECT file_name FROM file_details WHERE file_id = '"+arr[k]+"'" };
                serverLink temp1 = new serverLink();
                string tmp1 = temp1.sendToServer("POST", "/api/admin", headerKey1, headerVal1);
                string[] arr2 = tmp1.Split('"');

                var item = new ListViewItem(new[] { arr[k], arr[k + 2], arr2[5] });
                listView1.Items.Add(item);
                k += 4;
            }

        }

        public void loadListView()
        {
            listView1.View = View.Details;

            listView1.Columns.Add("File ID", 90, HorizontalAlignment.Left);
            listView1.Columns.Add("Download ID", 200, HorizontalAlignment.Left);
            listView1.Columns.Add("File Name", 200, HorizontalAlignment.Left);

            sqlStatement("SELECT file_id, download_id  FROM user_downloads WHERE user_id = '"+userid+"'");

            listView1.FullRowSelect = true;

        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
