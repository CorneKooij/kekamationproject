﻿using MovieBase_v0;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;  //Its for MySQL  
using file_transfer;
using System.IO;

namespace Kekamation_213
{
    public partial class Form1 : Form
    {
        private const string sqlSelectAll = "SELECT * FROM file_details ";
        private string emailGlobal;

        private TransferClient transferClient;
        private const string SERVER_IP = "34.240.65.169";//"34.253.45.82";
        private const int SERVER_PORT = 8001; //Open ports 8000-8020
        private Listener listener;
        private Timer tmrOverallProg;
        private string outputFolder;
        private string[] progressPath;
        private string downloadFile = "";

        public Form1()
        {
            
            emailGlobal = "test@test.com";
            InitializeComponent();  
        }

        public Form1(string email)
        {
            emailGlobal = email;
            string[] headerKey = { "sql" };
            string[] headerVal = { "SELECT user_isAdmin FROM user_details WHERE user_email = '" + email + "'" };
            serverLink temp = new serverLink();
            string tmp = temp.sendToServer("POST", "/api/admin", headerKey, headerVal);
            /*if (!tmp.Contains("0"))
                adminSettingsToolStripMenuItem.Enabled = true; -> null ref exceptions orals
            else
                transferSettingsToolStripMenuItem.Enabled = false; */

            InitializeComponent();
            progressPath = Directory.GetFiles(Application.StartupPath, "*.txt");
            tmrOverallProg = new System.Windows.Forms.Timer();
            tmrOverallProg.Interval = 1000;
            tmrOverallProg.Tick += tmrOverallProg_Tick;
            
        }

        //everytime the timer ticks it checks the tc
        private void tmrOverallProg_Tick(object sender, EventArgs e)
        {
            StreamReader file = new StreamReader("Progress");
            string prognum = file.ReadLine();
            pgbDownload.Value = Convert.ToInt32(prognum);
            file.Close();
            if (pgbDownload.Value == 100)
            {
                MessageBox.Show("Download Complete");
                string[] headerKey = { "sql" };
                string[] headerVal = { "INSERT INTO user_download (file_id, user_id) VALUES ("+downloadFile+","+getUserID(emailGlobal)+"); " };
                serverLink temp = new serverLink();
                string tmp = temp.sendToServer("POST", "/api/admin", headerKey, headerVal);
                tmrOverallProg.Stop();
            }             
        }

        private void newUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmNewAccount newAcc = new frmNewAccount();
            newAcc.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            frmUploadFile uploadFile;
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Multiselect = true;
            if(ofd.ShowDialog()== DialogResult.OK)
            {
                uploadFile = new frmUploadFile(ofd.FileName, emailGlobal);
           //     MessageBox.Show("File name: " + ofd.SafeFileName);
                uploadFile.Show();      
            }
        }

        private void splashToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmSplash splash = new frmSplash();
            splash.Show();
        }

        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmContentFilter filter = new frmContentFilter();
            filter.ShowDialog();
            sqlStatement(sqlSelectAll + "where File_name LIKE '%" + tbxSearch.Text + "%'" + filter.Sql);
        }

        private void accountToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAccountInfo accountInfo = new frmAccountInfo(getUserID(emailGlobal));
            accountInfo.ShowDialog();
        }

        private void adminSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAdminInfo adminInfo = new frmAdminInfo();
            adminInfo.ShowDialog();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            loadListView();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            sqlStatement(sqlSelectAll + "where File_name LIKE '%" + tbxSearch.Text + "%'");
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void transferSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmTransferSettings transSettings = new frmTransferSettings();
            transSettings.ShowDialog();
        }

        public void sqlStatement(string query)
        {
            listView1.Items.Clear();

            string[] headerKey = { "sql" };
            string[] headerVal = {  query};
            serverLink temp = new serverLink();
            string tmp = temp.sendToServer("POST", "/api/admin", headerKey, headerVal);

            string[] arr = tmp.Split('"');
            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine("" + i + ": " + arr[i]);
            }
            int result = 0;

            int k = 4;
            while ((k + 18) < arr.Length && !arr[k].Equals("}]}"))
            {
                arr[k] = arr[k].Remove(0, 1);
                arr[k] = arr[k].Remove(arr[k].Length - 1, 1);
                arr[k + 2] = arr[k + 2].Remove(0, 1);
                arr[k + 2] = arr[k + 2].Remove(arr[k + 2].Length - 1, 1);
                var item = new ListViewItem(new[] { arr[k], arr[k + 2], arr[k + 5], arr[k + 9], arr[k + 13], arr[k + 17] });
                listView1.Items.Add(item);
                k += 20;
                result++;
            }

            lblResult.Text = result + " Results found!";
        }

        public void loadListView()
        {
            listView1.View = View.Details;
            
            listView1.Columns.Add("File ID", 90, HorizontalAlignment.Left);
            listView1.Columns.Add("File Uploader ID", 130, HorizontalAlignment.Left);
            listView1.Columns.Add("File Name", 200, HorizontalAlignment.Left);
            listView1.Columns.Add("File Size", 130, HorizontalAlignment.Left);
            listView1.Columns.Add("File Extension", 100, HorizontalAlignment.Left);
            listView1.Columns.Add("Date Added", 160, HorizontalAlignment.Left);

            sqlStatement("SELECT * FROM file_details");

            listView1.FullRowSelect = true;

        }

        private void listView1_Click(object sender, EventArgs e)
        {
        }

        private void listView1_DoubleClick(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
                if (MessageBox.Show("Download file number " + listView1.SelectedItems[0].ToString() + "?", "Confirm Download", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    downloadFile = listView1.SelectedItems[0].ToString();
                    string[] headerKey = { "user", "file" };
                    string[] headerVal = { getUserID(emailGlobal), listView1.SelectedItems[0].Text };
                    serverLink temp = new serverLink();
                    string tmp = temp.sendToServer("POST", "/api/newrequest", headerKey, headerVal);
                    string myPath = System.Reflection.Assembly.GetEntryAssembly().Location;
                    string myDir = System.IO.Path.GetDirectoryName(myPath);
                    System.Diagnostics.Process.Start(Application.StartupPath.ToString() + @"\file transfer\file transfer\bin\Debug\file transfer.exe");
                    tmrOverallProg.Start();
                    try
                    {
                        StreamWriter file = new StreamWriter("Progress");
                        file.WriteLine(0);
                        file.Close();
                    }
                    catch (Exception ee)
                    {
                        MessageBox.Show(ee.Message);
                    }
                }
        }

        public string getUserID(string user_email)
        {
            string[] headerKey = { "sql" };
            string[] headerVal = { "SELECT user_id FROM user_details WHERE user_email = '" + user_email + "';" };
            serverLink temp = new serverLink();
            string tmp = temp.sendToServer("POST", "/api/admin", headerKey, headerVal);
            string[] arr = tmp.Split('"');
            arr[4] = arr[4].Remove(0, 1);
            arr[4] = arr[4].Remove(arr[4].Length - 3, 3);
            return arr[4];
        }


        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmHelp help = new frmHelp();
            help.ShowDialog();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            loadListView();
        }

        private void downoadedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            downloadedlist temp = new downloadedlist(getUserID(emailGlobal));
            temp.ShowDialog();
        }
    }
}
