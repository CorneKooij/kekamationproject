﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kekamation_213
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //Closes the app stupid
            Application.Exit();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            String[] filePath;
            OpenFileDialog ofd = new OpenFileDialog();
            if(ofd.ShowDialog()==DialogResult.OK)
            {
                filePath = ofd.FileNames;
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            frmUploadFile uploadFrm = new frmUploadFile();
            uploadFrm.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmContentFilter filter = new frmContentFilter();
            filter.ShowDialog();
        }
    }
}
