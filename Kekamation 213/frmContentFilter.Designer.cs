﻿namespace Kekamation_213
{
    partial class frmContentFilter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnFilter = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.cbxFilter = new System.Windows.Forms.ComboBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.gbxSort = new System.Windows.Forms.GroupBox();
            this.rtnDesc = new System.Windows.Forms.RadioButton();
            this.rtnAsc = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.gbxSort.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnFilter
            // 
            this.btnFilter.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFilter.Location = new System.Drawing.Point(68, 195);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(84, 28);
            this.btnFilter.TabIndex = 0;
            this.btnFilter.Text = "Filter";
            this.btnFilter.UseVisualStyleBackColor = true;
            this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 18);
            this.label2.TabIndex = 3;
            this.label2.Text = "Filter by ";
            // 
            // cbxFilter
            // 
            this.cbxFilter.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cbxFilter.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cbxFilter.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbxFilter.FormattingEnabled = true;
            this.cbxFilter.Items.AddRange(new object[] {
            "Date",
            "File Name",
            "File Size",
            "File Type",
            "Uploaded By"});
            this.cbxFilter.Location = new System.Drawing.Point(15, 64);
            this.cbxFilter.Name = "cbxFilter";
            this.cbxFilter.Size = new System.Drawing.Size(182, 26);
            this.cbxFilter.Sorted = true;
            this.cbxFilter.TabIndex = 4;
            this.cbxFilter.Text = "Date";
            // 
            // gbxSort
            // 
            this.gbxSort.Controls.Add(this.rtnDesc);
            this.gbxSort.Controls.Add(this.rtnAsc);
            this.gbxSort.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbxSort.Location = new System.Drawing.Point(15, 112);
            this.gbxSort.Name = "gbxSort";
            this.gbxSort.Size = new System.Drawing.Size(182, 77);
            this.gbxSort.TabIndex = 5;
            this.gbxSort.TabStop = false;
            this.gbxSort.Text = "Sort by";
            // 
            // rtnDesc
            // 
            this.rtnDesc.AutoSize = true;
            this.rtnDesc.Location = new System.Drawing.Point(33, 47);
            this.rtnDesc.Name = "rtnDesc";
            this.rtnDesc.Size = new System.Drawing.Size(104, 22);
            this.rtnDesc.TabIndex = 1;
            this.rtnDesc.TabStop = true;
            this.rtnDesc.Text = "Descending";
            this.rtnDesc.UseVisualStyleBackColor = true;
            this.rtnDesc.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // rtnAsc
            // 
            this.rtnAsc.AutoSize = true;
            this.rtnAsc.Location = new System.Drawing.Point(33, 23);
            this.rtnAsc.Name = "rtnAsc";
            this.rtnAsc.Size = new System.Drawing.Size(94, 22);
            this.rtnAsc.TabIndex = 0;
            this.rtnAsc.TabStop = true;
            this.rtnAsc.Text = "Ascending";
            this.rtnAsc.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(185, 18);
            this.label1.TabIndex = 6;
            this.label1.Text = "Choose a filter method:";
            // 
            // frmContentFilter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(216, 231);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.gbxSort);
            this.Controls.Add(this.cbxFilter);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnFilter);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmContentFilter";
            this.Text = "File Filter";
            this.gbxSort.ResumeLayout(false);
            this.gbxSort.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnFilter;
        private System.Windows.Forms.Label label2;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.GroupBox gbxSort;
        private System.Windows.Forms.RadioButton rtnDesc;
        private System.Windows.Forms.RadioButton rtnAsc;
        private System.Windows.Forms.ComboBox cbxFilter;
        private System.Windows.Forms.Label label1;
    }
}