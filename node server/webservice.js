var express = require('express');
var mysql = require('mysql');
var randtoken = require('rand-token');
var formidable = require('formidable');
var fs = require('fs');
var moment = require('moment');

const app = express();

var con = mysql.createPool({
  host: "34.240.65.169",
  user: "philip",
  password: "itrw225",
  database: "mydb"
});

app.get('/test', function(req, res){
  console.log('test');
  res.json({
    test: 'test success',
  });
});

app.post('/api/login', function(req, res){
  console.log("/api/login");
  var sql = 'SELECT * FROM user_details WHERE user_email = ? AND user_password = ?';
  var valP = req.headers["email"];
  var valU = req.headers["password"];
  console.log('name: ' + valP, ' pass: ' + valU);

  con.query(sql, [valP, valU], function (err, result) {
    if (err) res.json({result: 'Something went wrong (error)'});
    if(result[0] == undefined) res.json({login: "failed"});
	  else{
      if (result[0].user_isActivated !== 0) {
        console.log(result[0].name);
          const user = { id: result[0].name };
          res.json({
            login: 'success',
          });
      }
      else {
        res.json({
          login: "email not registered",
          username: result[0].username
        });
      }
	  }
  });
});

app.post('/api/registeruser', function(req, res){
  console.log("/api/registeruser");
  var emailAddress = req.headers['email'];
  var hash = randtoken.generate(16);
  var sql = 'INSERT INTO user_details (user_name, user_surname, user_email, user_password, user_isActivated, user_hash, user_isAdmin) VALUES ?';
  var val = [[
    req.headers["name"],
    req.headers["surname"],
    req.headers["email"],
    req.headers["password"],
    0,
    hash,
    0
  ]];

  con.query(sql, [val], function (err, result) {
    if (err) res.json({
		  registered: 'failed',
		  error: err
        });
    console.log('inserted val: ' + val);

    var link = hash;
    var email = require('./app/email')(link,emailAddress);

    res.json({
    register: "success",
    });
  });
});

app.get('/api/activate', function(req, res){
  console.log("/api/activate");
  var hash = req.query.hash;
  var email = req.query.email;
  var sql = 'SELECT user_hash FROM user_details WHERE user_email = ?;';
  con.query(sql, email, function(err, result){
    if(err) res.json({
		  result: err
    });
    else if (result[0].emailHash === hash){
      sql = 'UPDATE users SET isActivated = 1 WHERE email = ?';
      con.query(sql,email,function(err, result){
      if(err) res.json({
  		  result: err
      });
      else res.json({
        result: 'Successfully verified account!'
      });
      });
    }
    else res.json({
		  result: 'Something went wrong',
    });
  });
});

app.post('/api/checkemail', function(req, res){
  console.log("/api/checkemail");
  var emailA = req.headers["email"];
  var sql = 'SELECT user_email FROM user_details WHERE user_email = ?';
  if (emailA !== undefined) {
    con.query(sql, emailA, function (err, result) {
      if (err)  res.json({
            error: err
          });
      if(result[0] === undefined){
        res.json({
          email: 'available'
        });
      }
      else {
        res.json({
          email: 'taken'
        });
      }
    });
  }
  else {
    res.json({
          error: "error"
    });
  }
});

app.post('/api/resendemail', function(req, res){
  console.log("/api/resendemail");
  var user = req.headers["email"];
  var sql = 'SELECT user_hash, user_email FROM user_details WHERE user_email = ?';
  if(user !== undefined || user !== ""){
	con.query(sql, user, function(err, result){
    if (err) {res.json({error: err});}
    if (result[0] === undefined) {res.json({error: "user doesn't exist"});}
    else {
      var link = result[0].user_hash;
      var resend = require('./app/email')(link,result[0].user_email);
      res.json({status: "email send"});
    }
  });
  }
  else {res.json({error: "welp"});}
})

app.post('/api/newfile', function(req, res){
  console.log("/api/newfile");
  var mysqlTimestamp = moment(Date.now()).format('YYYY-MM-DD HH:mm:ss');
  var sql = 'INSERT INTO file_details (user_details_user_id, file_name, File_size, file_extension) VALUES ?';
  var val = [[
    req.headers["user"],
    req.headers["name"],
    req.headers["size"],
    req.headers["extension"]
  ]];

  con.query(sql, [val], function (err, result) {
    if (err) res.json({
		  registered: 'failed',
		  error: err
        });
    console.log('inserted val: ' + val);
    res.json({
    register: "success",
    });
  });
});

app.post('/api/newrequest', function(req, res){
  console.log("/api/newrequest");

  var sql = 'INSERT INTO user_requests (user_id, file_id) VALUES ?';
  var val = [[
    req.headers["user"],
    req.headers["file"],
  ]];

  con.query(sql, [val], function (err, result) {
    if (err) res.json({
		  newrequest: 'failed',
		  error: err
        });
    console.log('inserted val: ' + val);
    res.json({
    newrequest: "success",
    });
  });
});

app.post('/api/newdownload', function(req, res){
  console.log("/api/newdownload");

  var sql = 'INSERT INTO user_download (file_id, user_id, download_start_time) VALUES ?';
  var val = [[
    req.headers["file"],
    req.headers["user"],
    Date.now()
  ]];

  con.query(sql, [val], function (err, result) {
    if (err) res.json({
		  registered: 'failed',
		  error: err
        });
    console.log('inserted val: ' + val);
    res.json({
    newdownload: "success",
    });
  });
});

app.post('/api/admin', function(req, res){
  console.log("/api/admin");
  con.query(req.headers["sql"],function(err, result){
    if (err) res.json({result: err});
    res.json({
      result: result,
    });
  });
});

app.get('/api/getfiles', function(req, res){
  console.log('/api/getfiles');
  var files = "";
  fs.readdirSync(__dirname + '/Transfers').forEach(file => {
    files += file + ",";
  })
  res.json({folderContents: files});
});

app.get('/api/requestNotChecked', function(req, res){
  console.log('/api/requestNotChecked');
  var sql = 'SELECT file_id, request_id FROM user_requests WHERE request_checked = 0';
  con.query(sql, function (err, result) {
    if (err) res.json({  error: err  });
    else if(result[0] !== undefined)
      res.json({result: result})
  });
});

app.get('/api/getfiledetails', function(req, res){
  console.log('/api/getfiledetails');
  var val = req.headers['file_id'];
  var sql = 'SELECT file_name, file_extension FROM file_details WHERE file_id = ?;'
  con.query(sql,val, function(err, resul){
    if(err) res.json({error: err});
    res.json({result: resul});
  });
});

app.post('/api/requestchecked', function(req, res){
  console.log('/api/requestchecked');
  var val = req.headers['request_id'];
  console.log(val);
  var sql = "UPDATE user_requests SET request_checked = 1 WHERE request_id = ?";
  con.query(sql,val, function(err, resul){
    if(err) res.json({error: err});
    else {
      res.json({result: "success"});
    }
  });
});

app.listen(8080, function(){
  console.log('App is listening on port 8080!');
});
