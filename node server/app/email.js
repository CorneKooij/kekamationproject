var nodemailer = require('nodemailer');

var transporter = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: 'file.trans.email@gmail.com',
    pass: 'DatBoi420!'
  }
});

module.exports = function(link, emailAddress){
  var mailOptions = {
    from: 'file.trans.email@gmail.com',
    to: emailAddress,
    subject: 'Verification',
	  text: 'To verify your account, please go to this link: http://34.240.65.169:8080/api/activate?hash='
    + link + '&email=' + emailAddress
  };

  transporter.sendMail(mailOptions, function(error, info){
	if (error) {
      console.log(error);
	} else {
      console.log('Email sent: ' + info.response);
	}
  });
}
